var express = require('express');
var router = express.Router();
var functions = require('../../utils/functions')
var Support = require("../models/support_ticket");

/* GET users listing. */
router.post('/', function(req, res, next) {
    var message = req.body.message,
        subject = req.body.subject,
        email = req.body.email
    functions.reqUserAll(req, function(doc) {
        if(doc) {
            if(message != "" && subject != "" && email != "") {
                new Support({
                    author: doc._id,
                    message: message,
                    subject: subject
                }).save(function(err, obj) {
                    if(err) {
                        req.flash("error", "Something went wrong!")
                        res.redirect("/panel/support")
                    } else {
                        res.redirect("/panel/support/ticket/" + obj._id)
                    }
                })
            } else {
                req.flash("error", "Please enter all fields correctly.")
                res.redirect("/panel/support")
            }
        }
    });
});

module.exports = router;
