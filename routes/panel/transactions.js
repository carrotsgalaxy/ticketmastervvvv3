var express = require('express');
var router = express.Router();
var functions = require('../../utils/functions')

/* GET users listing. */
router.get('/', function(req, res, next) {
  var stripe = req.app.locals.stripe;
  functions.reqUser(req, function(doc) {
    var promise = new Promise(function(resolve, reject) {
      if(doc.customerToken) {
        stripe.charges.list(
          {customer : doc.customerToken},
          function(err, transactions) {
            if(!err) {
              resolve(transactions.data)
            } else {
              reject(err)
            }
        });        
      } else {
        resolve([])
      }
    })
    promise.then(function(resolved) {
      res.render('user/transactions', { 
        title: 'Transactions', 
        description: 'View all your transactions and there details.',
        route: req.originalUrl,
        error: req.flash('error'),
        success: req.flash('success'),
        user: doc,
        transactions: resolved
      });
    }).catch(function(reject) {
      res.redirect("/")
    })
  });
});

module.exports = router;
