var mongoose = require('mongoose');
var bcrypt = require("bcrypt")
var Schema = mongoose.Schema;

var ticket = new Schema({
  buyer: String,
  author: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'author'
  },
  user_author: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'user_author'
  },
  ticket: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'ticket'
  },
  created_at: Date,
  txn_id: String,
  updated_at: Date
});

ticket.pre('save', function(next) {
  // get the current date
  var currentDate = new Date();

  // change the updated_at field to current date
  this.updated_at = currentDate;

  // if created_at doesn't exist, add to that field
  if (!this.created_at) this.created_at = currentDate;
  
  next()

});

ticket.set('collection', 'tickets');

var Ticket = mongoose.model('Ticket', ticket);

// make this available to our events in our Node applications
module.exports = Ticket;