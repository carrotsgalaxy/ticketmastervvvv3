var express = require('express');
var router = express.Router();
var functions = require('../../utils/functions')
var validate = require("validate.js")
var Event = require("../models/event")
var formidable = require('formidable');

router.post("/update_card", (req, res) => {
  var id = req.params.id;
  var stripe = req.app.locals.stripe;
  var token = req.body.stripeToken;
  functions.getUser(req.session.email, function(doc) {
    if(doc) {
      stripe.accounts.createExternalAccount(
        doc.stripeCustomerToken,
        {external_account: token},
        function(err, bank_account) {
          if(err) {
            console.log(err)
            req.flash("error", "Something went wrong, try again.")
            res.redirect("/panel/withdraw")       
          } else {
            req.flash("success", "Successfully changed card details")
            res.redirect("/panel/withdraw")           
          }
        }
      );
      
    } else {
      res.redirect("/")
    }
  })
})


router.get("/cancel_sub/:id", (req, res) => {
  var id = req.params.id;
  var stripe = req.app.locals.stripe;

  functions.getUser(req.session.email, function(doc) {
    if(doc) {
      functions.verifySubscription(doc, id, req, function(err, match) {
        if(!err && match) {
          stripe.subscriptions.del(
            id,
            function(err, confirmation) {
              if(err) {
                console.log(err)
                req.flash("error", "Something went wrong!")
                res.redirect("/panel/plans")
              } else {
                req.flash("success", "Successfully cancelled subscription.")
                res.redirect("/panel/plans")
              }
            }
          );      
        } else {
          res.redirect("/panel/plans")         
        }
      })        
    } else {
      res.redirect("/panel/plans")   
    }
  })
})

var fs = require("fs")

router.post("/update_identity", (req, res) => {
  var stripe = req.app.locals.stripe;
  functions.getUser(req.session.email, function(doc) {
    if(doc) {
      if (!req.files || req.files.sampleFile == undefined) {
        req.flash("error", "Please make sure you upload a file.");
        res.redirect("/panel/withdraw")
        return;
      }
 
      let sampleFile = req.files.sampleFile;
     
      // Use the mv() method to place the file somewhere on your server
      sampleFile.mv('public/images/identities/identity_' +req.session.email + '.jpg' , function(err) {
        if (err)
          return res.status(500).send(err);
          
        stripe.fileUploads.create(
          {
            purpose: 'identity_document',
            file: {
              data: fs.readFileSync('public/images/identities/identity_' +req.session.email + '.jpg'),
              name: 'identity_' +req.session.email + '.jpg',
              type: 'application/octet-stream'
            }
          },
          {stripe_account: doc.stripeCustomerToken}
        ).then(function(result) {
          stripe.accounts.retrieve(
            doc.stripeCustomerToken,
            function(err, account) {
              if(!err && account && account.legal_entity.verification.document == null) {
                stripe.accounts.update(
                  doc.stripeCustomerToken,
                  {legal_entity: {verification: {document: result.id}}}
                ).then(function(acct) {
                  req.flash("success", "Successfully uploaded identity verification");
                  res.redirect("/panel/withdraw")
                });
              } else {
                req.flash("success", "Your account is already verified.");
                res.redirect("/panel/withdraw")
              }
            }
          );
        });     
        
      });
    }
  });
});

router.get("/collect_money", (req, res) => {
  var stripe = req.app.locals.stripe;
  functions.getUser(req.session.email, function(doc) {
    stripe.charges.create({
      amount: 1000,
      currency: "gbp",
      source: "tok_visa",
      destination: {
        account: doc.stripeCustomerToken,
      },
    }).then(function(charge) {
      res.redirect("/panel/withdraw")
    });
  })
})

router.post("/update_account", (req, res) => {
  var stripe = req.app.locals.stripe;
  functions.getUser(req.session.email, function(doc) {
    if(doc) {
      var obj = {
        address: {
          city: req.body.city,
          line1: req.body.line1,
          postal_code: req.body.postal_code,
          state: req.body.state
        },
        first_name: req.body.first_name,
        last_name: req.body.last_name
      }
      console.log(obj)
      stripe.accounts.update(doc.stripeCustomerToken, {legal_entity: obj}, function(err) {
        if(!err) {
          req.flash("success", "Successfully changed details.")
          res.redirect("/panel/account_settings")          
        } else {
          console.log(err)
          req.flash("error", err.message)
          res.redirect("/panel/account_settings")
        }
      })
    }
  });
});

router.post("/verify_account", (req, res) => {
  var stripe = req.app.locals.stripe;
  functions.getUser(req.session.email, function(doc) {
    if(doc) {
      stripe.accounts.retrieve(
        doc.stripeCustomerToken,
      function(err, account) {
        var obj = {}
        var promise;
        if(!err && account.legal_entity.verification.status == "unverified") {
            promise = new Promise(function(resolve, reject) {
              for(var key in req.body) {
                if(req.body[key] == "") {
                  req.flash("error", "Please complete all fields to verify your account")
                  res.redirect("/panel/withdraw")
                } else {
                  var k = key.split(".")
                  if(k.length == 2) {
                    obj[k[1]] = req.body[key]
                  } else if(k.length == 3) {
                    var one = k[1]
                    var two = k[2]
                    obj[one] = obj[one] == undefined ? {} : obj[one]
                    obj[one][two] = req.body[key]
                  }
                }
              }
              resolve()
            });
        } else if(!err && account.legal_entity.verification.status == "verified") {
          promise = new Promise(function(resolve, reject) {
            for(var key in req.body) {
              if(req.body[key] == "") {
                req.flash("error", "Please complete all fields to verify your account")
                res.redirect("/panel/withdraw")
              } else {
                var k = key.split(".")
                if(key != "legal_entity.ssn_last_4") {
                  if(k.length == 2) {
                    obj[k[1]] = req.body[key]
                  } else if(k.length == 3) {
                    var one = k[1]
                    var two = k[2]
                    obj[one] = obj[one] == undefined ? {} : obj[one]
                    obj[one][two] = req.body[key]
                  }
                }
              }
            }
            resolve()
          });
        }
          
        promise.then(function() {
          stripe.accounts.update(doc.stripeCustomerToken, {legal_entity: obj}, function(err) {
            if(!err) {
              req.flash("success", "Successfully changed details.")
              res.redirect("/panel/withdraw")          
            } else {
              console.log(err)
              req.flash("error", err.message)
              res.redirect("/panel/withdraw")
            }
          })
        })
      }); 
    }
  });
});

router.post("/charge", (req, res) => {
  var stripe = req.app.locals.stripe;
  functions.getUser(req.session.email, function(doc) {
    if(doc) {
      if(doc.customerToken != undefined) {
        stripe.subscriptions.create({
          customer: doc.customerToken,
          items: [
            {
              plan: "advanced",
            },
          ], card: req.body.stripeToken
        }, function(err, subscription) {
            if(!err && subscription) {
              req.flash("success", "You now have access to the advanced plan!")
              res.redirect("/panel/plans")   
            } else {
              console.log(err)
              req.flash("error", "Something went wrong!")
              res.redirect("/panel/plans")
            }
        });
      } else {
        stripe.customers.create(
          { email: doc.email, plan: "advanced", card: req.body.stripeToken },
          function(err, customer) {
            if (err) {
              console.log(err)
              req.flash("error", "Something went wrong!")
              res.redirect("/panel/plans")
              return;
            }
            doc.customerToken = customer.id;
            doc.save(function(error) {
                if(!error) {
                  req.flash("success", "You now have the advanced plan!")
                  res.redirect("/panel/plans")                      
                } else {
                  console.log(error)
                  req.flash("error", "Something went wrong!")
                  res.redirect("/panel/plans")
                }
            })
          }
        );           
      }
    } else {
      req.flash("error", "Could not find user")
      res.redirect("/panel/plans")
    }
  })
});


module.exports = router;
