var express = require('express');
var User = require("./models/user")
var router = express.Router();

/* GET home page. */
router.get('/:code/:email', function(req, res, next) {
    var code = req.params.code;
    var email = req.params.email;
    
    User.findOne({"email" : email}, function(err, doc) {
        console.log(doc)
        if(!err && doc) {
            if(code == doc.verification.code) {
                doc.verification.redeemed = true;
                doc.save(function(err) {
                    if(err) {
                        req.flash("error", "Something went wrong during account verification (ERROR: 1100)")
                        res.redirect("/")
                    } else {
                        req.flash("success", "Your account has been successfully verified.")
                        res.redirect("/login")
                    }
                })
            } else {
                req.flash("error", "Something went wrong during account verification (ERROR: 1000)")
                res.redirect("/")
            }
        } else {
            req.flash("error", "Something went wrong during account verification.")
            res.redirect("/")
        }
    })
});

module.exports = router;
